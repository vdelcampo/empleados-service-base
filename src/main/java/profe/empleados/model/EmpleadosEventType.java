package profe.empleados.model;


import java.io.Serializable;

public enum EmpleadosEventType implements Serializable {
	CREATE, DELETE, UPDATE
}
